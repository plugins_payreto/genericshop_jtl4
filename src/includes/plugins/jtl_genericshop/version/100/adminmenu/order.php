<?php
Jtllog::writeLog('debug log : render order page', JTLLOG_LEVEL_ERROR);
global $smarty, $oPlugin;

require_once(PFAD_ROOT . PFAD_INCLUDES . "tools.Global.php");
require_once(PFAD_ROOT . PFAD_CLASSES . "class.JTL-Shop.Bestellung.php");
require_once(PFAD_ROOT . PFAD_ADMIN . PFAD_INCLUDES . "blaetternavi.php");
require_once(PFAD_ROOT . PFAD_ADMIN . PFAD_INCLUDES . "bestellungen_inc.php");
require_once(PFAD_ROOT . PFAD_INCLUDES_MODULES . 'PaymentMethod.class.php');

$success = '';
$error = '';
$cSuchFilter = '';
$cSuche = '';

// navigation
$nAnzahlProSeite = 15;
$oBlaetterNaviConf = baueBlaetterNaviGetterSetter(1, $nAnzahlProSeite);

if (isset($_POST['submit_action'])) {
    $cOrderId = isset($_POST['orderId']) ? filterXSS($_POST['orderId']) : '';
    Jtllog::writeLog('debug log : get orderID : '.$cOrderId, JTLLOG_LEVEL_ERROR);

    $method = isset($_POST['paymentMethod']) ? filterXSS($_POST['paymentMethod']) : '';
    Jtllog::writeLog('debug log : payment method : '.$method, JTLLOG_LEVEL_ERROR);
    
    $action = isset($_POST['submit_action']) ? filterXSS($_POST['submit_action']) : '';
    Jtllog::writeLog('debug log : action : '.$action, JTLLOG_LEVEL_ERROR);

    $name = str_replace('genericshop_', '', $method);

    switch ($name) {
        case 'cc':
            $name = 'kreditkarte(normal)';
            break;
        case 'ccsaved':
            $name = 'kreditkarte(recurring)';
            break;
    }

    $oPlugin = Plugin::getPluginById('jtl_genericshop');

    $paymentMethod = PaymentMethod::create('kPlugin_'.$oPlugin->kPlugin.'_'.$name);
    $payment = $DB->executeQuery(
        "SELECT * from xplugin_jtl_genericshop_orders WHERE order_id = '"
            .intval($cOrderId)."'",
        1
    );
    Jtllog::writeLog('debug log : current order status : '.$payment->payment_type, JTLLOG_LEVEL_ERROR);

    if ($action == 'update') {
        $result = $paymentMethod->doUpdateStatus($cOrderId);
    } elseif ($action == 'capture') {
        Jtllog::writeLog('debug log : capture payment ', JTLLOG_LEVEL_ERROR);
        $result = $paymentMethod->doBackendPayment($cOrderId, 'CP' );
    } elseif ($action == 'refund') {
        Jtllog::writeLog('debug log : refund payment ', JTLLOG_LEVEL_ERROR);
        $result = $paymentMethod->doBackendPayment($cOrderId, 'RF');
    }

    Jtllog::writeLog('debug log : result from gateway : '.$result, JTLLOG_LEVEL_ERROR);

    if ($result) {
        $successMessage = 'SUCCESS_GENERAL_'.strtoupper($action).'_PAYMENT';
        $success = $oPlugin->oPluginSprachvariableAssoc_arr[$successMessage];
        echo $success;
    } else {
        $errorMessage = 'ERROR_'.strtoupper($action).'_BACKEND';
        $error = $oPlugin->oPluginSprachvariableAssoc_arr[$errorMessage];
        echo $error;
    }
     Jtllog::writeLog('debug log : end change order status', JTLLOG_LEVEL_ERROR);
     exit();
}

$inReviewStatus = array();

foreach ($oPlugin->oPluginSprachvariable_arr as $translation) {
    if ($translation->cName == 'BACKEND_BT_REVIEW') {
        $inReviewStatus = $translation->oPluginSprachvariableSprache_arr;
    }
}

// check for PA and update status payment.
$GLOBALS['DB']->executeQuery("UPDATE tbestellung INNER JOIN xplugin_jtl_genericshop_orders"
    . " ON tbestellung.kBestellung = xplugin_jtl_genericshop_orders.order_id SET "
    . "tbestellung.cStatus = '2' WHERE xplugin_jtl_genericshop_orders.payment_type = 'PA'", 4);

if (verifyGPCDataInteger('Suche') === 1) { // Bestellnummer gesucht
    $cSuche = StringHandler::filterXSS(verifyGPDataString('cSuche'));
    if (strlen($cSuche) > 0) {
        $cSuchFilter = "WHERE tbestellung.cBestellNr='$cSuche'";
    }
}

// orders
$nAnzahlBestellungen = $GLOBALS['DB']->executeQuery("SELECT tbestellung.kBestellung from "
    . "tbestellung INNER JOIN xplugin_jtl_genericshop_orders ON tbestellung.kBestellung "
    . "= xplugin_jtl_genericshop_orders.order_id $cSuchFilter", 3);
$oBestellungArr = $GLOBALS['DB']->executeQuery("SELECT tbestellung.kBestellung, "
    ."xplugin_jtl_genericshop_orders.payment_method, xplugin_jtl_genericshop_orders.transaction_id, "
    . "xplugin_jtl_genericshop_orders.payment_type, xplugin_jtl_genericshop_orders.status "
    . "from tbestellung INNER JOIN xplugin_jtl_genericshop_orders ON tbestellung.kBestellung = "
    . "xplugin_jtl_genericshop_orders.order_id $cSuchFilter ORDER BY kBestellung "
    . "DESC {$oBlaetterNaviConf->cSQL1}", 2);

// fill
foreach ($oBestellungArr as &$oBestellung) {
    $paymentMethod = $oBestellung->payment_method;
    $transactionId = $oBestellung->transaction_id;
    $paymentType   = $oBestellung->payment_type;
    if ($oBestellung->status == $inReviewStatus['GER'] || $oBestellung->status == $inReviewStatus['ENG']) {
        $paymentType = 'IR';
    }
    $oBestellung = new Bestellung($oBestellung->kBestellung);
    if ($paymentMethod == 'genericshop_ccsaved') {
        $oBestellung->cZahlungsartName .= ' (Recurring)';
    }
    $oBestellung->cPaymentMethod = $paymentMethod;
    $oBestellung->cTransactionId = $transactionId;
    $oBestellung->cPaymentType   = $paymentType;
    $oBestellung->fuelleBestellung(1, 0);
}

// navigation
$oBlaetterNaviUebersicht = baueBlaetterNavi(
    $oBlaetterNaviConf->nAktuelleSeite1,
    $nAnzahlBestellungen,
    $nAnzahlProSeite
);

$pluginPath = gibShopURL() . "/" . PFAD_PLUGIN . $oPlugin->cVerzeichnis
    . "/" . PFAD_PLUGIN_VERSION . $oPlugin->nVersion . "/paymentmethod";

// template
$smarty->assign('cHinweis', $success)
       ->assign('cFehler', $error)
       ->assign('cSuche', $cSuche)
       ->assign('oBestellung_arr', $oBestellungArr)
       ->assign("oBlaetterNaviUebersicht", $oBlaetterNaviUebersicht)
       ->assign('adminURL', Shop::getURL() . '/' . PFAD_ADMIN . 'plugin.php')
       ->assign('adminURLParam', 'kPlugin=' . $oPlugin->kPlugin)
       ->assign('pluginPath', $pluginPath)
       ->display($oPlugin->cAdminmenuPfad . "template/order.tpl");
