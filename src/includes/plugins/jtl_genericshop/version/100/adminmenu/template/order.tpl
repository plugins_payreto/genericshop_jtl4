{config_load file="$lang.conf" section="bestellungen"}
<style>
.overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #000;
    opacity: 0.6;
    z-index: 999999
}
.dialog {
    position: absolute;
    width: 562px;
    margin: -100px auto 0 auto;
    top: 50%;
    left: 0;
    right: 0;
    bottom: 0;
   
}
</style>

{literal}
<script type="text/javascript">
function showConfirmationAlert() { /*change*/
        var $content =  "<div class='overlay'>" +
                        "<div class='dialog'>"+
                        "<img src='{/literal}{$pluginPath}{literal}/images/loader.gif'>"+
                        "</div>" +
                        "</div>";
         $('body').prepend($content);
      
}

function closeAlert() {
    $('.dialog-ovelay').hide();
}

function changeStatus(submit_action, orderId, paymentMethod) {
    showConfirmationAlert();
    $.post("{/literal}{$adminURL}?{$adminURLParam}{literal}",
    {
        submit_action : submit_action,
        orderId : orderId,
        paymentMethod : paymentMethod
    },
    function(data,status){
        closeAlert();
        alert(data);
        location.href = window.location.href;
        
    })
    .fail(function() {
        alert( "error when try to "+submit_action);
    });
}

</script>
{/literal}

<div id="content" class="container-fluid">
    <div class=" block clearall">
        <div class="left">
            {if isset($cSuche) && $cSuche|count_characters > 0}
                {assign var=pAdditional value="&cSuche="|cat:$cSuche}
            {else}
                {assign var=pAdditional value=''}
            {/if}
            {include file='pagination.tpl' cSite=1 cUrl=$adminURL oBlaetterNavi=$oBlaetterNaviUebersicht cParams="&`$adminURLParam``$pAdditional`" hash=''}
        </div>
        <div class="right">
            <form name="bestellungen" method="post" action="{$adminURL}?{$adminURLParam}">
                {$jtl_token}
                <input type="hidden" name="Suche" value="1" />
                <div class="input-group">
                    <span class="input-group-addon">
                        <label for="orderSearch">{#orderSearchItem#}:</label>
                    </span>
                    <input class="form-control" name="cSuche" type="text" value="{if isset($cSuche)}{$cSuche}{/if}" id="orderSearch" />
                    <span class="input-group-btn">
                        <button name="submitSuche" type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Suchen</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
    {if $oBestellung_arr|@count > 0 && $oBestellung_arr}
        <form name="bestellungen" method="post" action="{$adminURL}?{$adminURLParam}">
            {$jtl_token}
            {if isset($cSuche) && $cSuche|count_characters > 0}
                <input type="hidden" name="cSuche" value="{$cSuche}" />
            {/if}
        </form>
        <div class="panel panel-default" style="overflow: auto">
            <div class="panel-heading">
                <h3 class="panel-title">{#order#}</h3>
            </div>
            <table class="list table">
                <thead>
                <tr>
                    <th class="tleft">{#orderNumber#}</th>
                    <th class="tleft">{#orderCostumer#}</th>
                    <th class="tleft">{#orderPaymentName#}</th>
                    <th class="tleft">{$oPlugin->oPluginSprachvariableAssoc_arr.BACKEND_TT_TRANSACTION_ID}</th>
                    <th>{#orderSum#}</th>
                    <th>Status</th>
                    <th class="tcenter">{#orderDate#}</th>
                    <th class="tcenter">Aktion</th>
                </tr>
                </thead>
                <tbody>
                {foreach name=bestellungen from=$oBestellung_arr item=oBestellung}
                    <tr class="tab_bg{$smarty.foreach.bestellungen.iteration%2}">
                        <td>{$oBestellung->cBestellNr}</td>
                        <td>{if $oBestellung->oKunde->cVorname || $oBestellung->oKunde->cNachname || $oBestellung->oKunde->cFirma}{$oBestellung->oKunde->cVorname} {$oBestellung->oKunde->cNachname}{if isset($oBestellung->oKunde->cFirma) && $oBestellung->oKunde->cFirma|count_characters > 0} ({$oBestellung->oKunde->cFirma}){/if}{else}{#noAccount#}{/if}</td>
                        <td>{$oBestellung->cZahlungsartName}</td>
                        <td>{$oBestellung->cTransactionId}</td>
                        <td class="tcenter">{$oBestellung->WarensummeLocalized[0]}</td>
                        <td class="tcenter status{$oBestellung->kBestellung}">{if $oBestellung->cPaymentType == 'IR'}{$oPlugin->oPluginSprachvariableAssoc_arr.BACKEND_BT_REVIEW}{elseif $oBestellung->cPaymentType == 'PA'}{$oPlugin->oPluginSprachvariableAssoc_arr.BACKEND_TT_PA}{elseif $oBestellung->cPaymentType == 'DB'}{$oPlugin->oPluginSprachvariableAssoc_arr.BACKEND_TT_ACC}{elseif $oBestellung->cPaymentType == 'CP'}{$oPlugin->oPluginSprachvariableAssoc_arr.BACKEND_TT_ACC}{elseif $oBestellung->cPaymentType == 'RF'}{$oPlugin->oPluginSprachvariableAssoc_arr.GENERAL_TEXT_REFUNDED}{/if}</td>
                        <td class="tcenter">{$oBestellung->dErstelldatum_de}</td>
                        <td class="tcenter">
                           
                                {$jtl_token}
                                
                        
                                {if $oBestellung->cPaymentType == 'IR'}
                                    <button onclick="changeStatus('update', '{$oBestellung->kBestellung}', '{$oBestellung->cPaymentMethod}')" class="btn btn-success">{$oPlugin->oPluginSprachvariableAssoc_arr.BACKEND_BT_UPDATE_ORDER}
                                    </button>
                                {elseif $oBestellung->cPaymentType == 'PA'}
                                    <button onclick="changeStatus('capture', '{$oBestellung->kBestellung}', '{$oBestellung->cPaymentMethod}')" class="btn btn-info">{$oPlugin->oPluginSprachvariableAssoc_arr.BACKEND_BT_CAPTURE}
                                    </button>
                                {elseif $oBestellung->cPaymentType == 'DB' || $oBestellung->cPaymentType == 'CP'}
                                    <button onclick="changeStatus('refund', '{$oBestellung->kBestellung}', '{$oBestellung->cPaymentMethod}')" class="btn btn-warning">{$oPlugin->oPluginSprachvariableAssoc_arr.BACKEND_BT_REFUND}
                                    </button>
                                {/if}
                          
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    {else}
        <div class="alert alert-info"><i class="fa fa-info-circle"></i> Keine Daten vorhanden.</div>
    {/if}
</div>
