CREATE TABLE `xplugin_jtl_genericshop_recurring` (
	`id` INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`cust_id` INT(10) NOT NULL,
	`payment_group` VARCHAR(32),
	`brand` VARCHAR(100),
	`holder` VARCHAR(100) NULL default NULL,
	`email` VARCHAR(100) NULL default NULL,
	`last4digits` VARCHAR(4),
	`expiry_month` VARCHAR(2),
	`expiry_year` VARCHAR(4),
	`server_mode` VARCHAR(4) NOT NULL,
	`channel_id` VARCHAR(32) NOT NULL,
	`ref_id` VARCHAR(32),
	`payment_default` boolean NOT NULL default '0'
);

CREATE TABLE `xplugin_jtl_genericshop_orders` (
	`id` INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`order_id` INT(10) NOT NULL,
	`transaction_id` VARCHAR(32) NOT NULL,
	`ref_id` VARCHAR(32) NOT NULL,
	`payment_method` VARCHAR(100) NOT NULL,
	`payment_name` VARCHAR(200) NOT NULL,
	`amount` double NOT NULL,
	`currency` VARCHAR(3) NOT NULL,
	`mandate_id` VARCHAR(100) NOT NULL,
	`mandate_date` VARCHAR(10) NOT NULL,
	`payment_type` VARCHAR(2) NOT NULL,
	`status` VARCHAR(100) NOT NULL
);

ALTER TABLE `tzahlungsart` CHANGE `cBild` `cBild` TEXT;