<?php
require_once('core.php');
class VersionTracker extends GenericshopPaymentCore
{
    private static $versionTrackerUrl = 'http://api.dbserver.payreto.eu/v1/tracker';

    /**
    * provide version tracker api url
    *
    * @return string $versionTrackerUrl
    */
    private static function getVersionTrackerUrl()
    {
        return self::$versionTrackerUrl;
    }

    /**
    * request version tracker parameters
    *
    * @return array $data
    */
    private static function getVersionTrackerParameter($versionData)
    {
        $versionData['hash'] =
            md5($versionData['shop_version'].
            $versionData['plugin_version'].
            $versionData['client']);

        return http_build_query(array_filter($versionData), '', '&');
    }

    /**
    * send request to version tracker api
    *
    * @return array|boolean
    */
    public static function sendVersionTracker($versionData)
    {
        global $oPlugin;
        $postData = self::getVersionTrackerParameter($versionData);
        $url      = self::getVersionTrackerUrl();
        return self::getResponseData($postData, $url, $versionData['server_mode']);
    }
}
