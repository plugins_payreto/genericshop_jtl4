<?php
// header("Content-Type: text/html; charset=utf-8");
/**
 * @copyright (c) JTL-Software-GmbH
 * @license http://jtl-url.de/jtlshoplicense
 */

include_once(PFAD_ROOT.PFAD_INCLUDES_MODULES.'PaymentMethod.class.php');
include_once(dirname(__FILE__).'/core.php');
include_once(dirname(__FILE__).'/versiontracker.php');

/**
 * Class Genericshop.
 */
class Genericshop extends PaymentMethod
{
    protected $client      = 'Genericshop';
    protected $shopSystem = 'JTL';
    protected $testMode    = 'EXTERNAL';

    /**
    * initial payment method
    *
    * @param int $nAgainCheckout
    * @param boolean $mainPage
    */
    public function init($nAgainCheckout = 0)
    {
        parent::init($nAgainCheckout);
        // set name for payment method.
        $this->name = substr($this->moduleID, strripos($this->moduleID, '_') + 1);
        $name = str_replace('(normal)', '', $this->name);
        $name = str_replace('(recurring)', 'saved', $name);
        $name = str_replace('kreditkarte', 'cc', $name);

        $this->paymentMethod = 'genericshop_'.$name;
        $this->userId = 0;
        if (isset($_SESSION['Kunde'])) {
            $this->userId = $_SESSION['Kunde']->kKunde;
        }
    }

    /**
    * provide config value
    *
    * @param string $key
    * @return string
    */
    public function getConfig($key)
    {
        global $oPlugin;

        if (isset($oPlugin->oPluginEinstellungAssoc_arr[$this->moduleID.'_'.$key])) {
            return $oPlugin->oPluginEinstellungAssoc_arr[$this->moduleID.'_'.$key];
        }
        return '';
    }

    /**
    * provide general config value
    *
    * @param string $key
    * @return string
    */
    public function getConfigGeneral($key)
    {
        global $oPlugin;

        return $oPlugin->oPluginEinstellungAssoc_arr['genericshop_general_'.$key];
    }

    /**
    * provide customer data
    *
    * @param object $order
    * @return array $transactionData
    */
    public function getCustomerParameters($order)
    {
        $transactionData = array();
        $transactionData['customer']['email'] = $order->oRechnungsadresse->cMail;
        $transactionData['customer']['first_name'] = $this->convertToUtf8($order->oRechnungsadresse->cVorname);
        $transactionData['customer']['last_name'] = $this->convertToUtf8($order->oRechnungsadresse->cNachname);
        $transactionData['billing']['street'] = $this->convertToUtf8($order->oRechnungsadresse->cStrasse) . ' '
            . $this->convertToUtf8($order->oRechnungsadresse->cHausnummer);
        $transactionData['billing']['city'] = $this->convertToUtf8($order->oRechnungsadresse->cOrt);
        $transactionData['billing']['zip'] = $order->oRechnungsadresse->cPLZ;
        $transactionData['billing']['country_code'] = $order->oRechnungsadresse->cLand;
        $transactionData['amount'] = $this->getAmountByOrderWarensummeLocalized($order->WarensummeLocalized);
        $transactionData['currency'] =  $order->Waehrung->cISO;
        $transactionData['customer_ip'] = $this->getCustomerIp();

        return $transactionData;
    }

    /**
     * get amount by Order WarensummeLocalized
     *
     * @param      array  $orderWarensummeLocalized
     *
     * @return     string
     */
    protected function getAmountByOrderWarensummeLocalized($orderWarensummeLocalized)
    {
        $amount = explode(' ', $orderWarensummeLocalized[0]);
        $amount = str_replace('.', '', $amount[0]);
        $amount = str_replace(',', '.', $amount);
        return $amount;
    }

    /**
     * convert to UTF 8
     *
     * @param      string  $parameter  The parameter
     *
     * @return     string
     */
    public function convertToUtf8($parameter)
    {
        if ($this->isCustomerLogin() == 'true') {
            return utf8_encode($parameter);
        }

        return html_entity_decode($parameter, ENT_COMPAT, 'utf-8');
    }

    /**
    * provide cc3d parameters
    *
    * @param string $amount
    * @param string $currency
    * @return array $transactionData
    */
    public function getCC3DParameters($amount, $currency)
    {
        $transactionData = array();
        if ($this->paymentMethod == 'genericshop_ccsaved') {
            $transactionData['3D']['amount'] = $amount;
            $transactionData['3D']['currency'] = $currency;
        }
        return $transactionData;
    }

    /**
     * Check customer login status and return as string
     *
     * @return string
     */
    protected function isCustomerLogin()
    {
        if (!empty($_SESSION['Kunde']->kKunde)) {
            return 'true';
        }
        return 'false';
    }


    /**
     * Get customer created date
     *
     * @return string|boolean
     */
    protected function getCustomerCreatedDate()
    {
        if ($this->isCustomerLogin() == 'true') {
            return $_SESSION['Kunde']->dErstellt;
        }
        return date("Y-m-d");
    }


    /**
     * Get customer total order  by complete status
     *
     * @param array $order_info
     * @return  array
     */
    public function getTotalOrdersByCompleteStatus()
    {
        if ($this->isCustomerLogin() == 'true') {
            $customerTotalOrderByCompleteStatus = (int)Shop::DB()->query(
                "SELECT COUNT(*)  as totalOrder FROM `tbestellung` WHERE kKunde = ".
                $_SESSION['Kunde']->kKunde,
                1
            )->totalOrder;
        } else {
            $customerTotalOrderByCompleteStatus = 0;
        }

        return $customerTotalOrderByCompleteStatus;
    }


    /**
     * Get risk kunden status
     *
     * @return string|boolean
     */
    protected function getRiskKundenStatus()
    {
        if ($this->getTotalOrdersByCompleteStatus() > 0) {
            return 'BESTANDSKUNDE';
        }
        return 'NEUKUNDE';
    }


    /**
    * provide registration payment account parameters
    *
    * @return array $transactionData
    */
    public function getRegistrationParameters()
    {
        $transactionData = array();
        if ($this->isRecurring()) {
            $transactionData['payment_registration'] = 'true';

            $registrations = $this->getRegistrations();
            if ($registrations) {
                foreach ($registrations as $key => $value) {
                    $transactionData['registrations'][$key] = $value['ref_id'];
                }
            }
        }
        return $transactionData;
    }

    /**
    * Get payment widget from gateway
    *
    * @param object $order
    * @return string $paymentWidget
    */
    public function getPaymentWidget($order)
    {
        $transactionData = array_merge_recursive(
            $this->getCredentials(),
            $this->getCustomerParameters($order),
            $this->getCC3DParameters(
                $this->getAmountByOrderWarensummeLocalized($order->WarensummeLocalized),
                $order->Waehrung->cISO
            ),
            $this->getRegistrationParameters()
        );
        $transactionData['test_mode']    = $this->getTestMode();
        $transactionData['payment_type'] = $this->getPaymentType();
        $_SESSION['genericshop']['transaction_id'] = date('YmdHis').$this->randomNumber(5);
        $transactionData['transaction_id'] = $_SESSION['genericshop']['transaction_id'];

        $_SESSION['genericshop']['transaction_data'] = $transactionData;
        $checkoutResult = GenericshopPaymentCore::getCheckoutResult($transactionData);
        if (!$checkoutResult['isValid'] || !isset($checkoutResult['response']['id'])) {
            $paymentWidget ['isValid'] = false;
            $paymentWidget ['response'] = 'ERROR_GENERAL_REDIRECT';
        } else {
            $paymentWidgetUrl = GenericshopPaymentCore::getPaymentWidgetUrl(
                $transactionData,
                $checkoutResult['response']['id']
            );
            $paymentWidgetContent = GenericshopPaymentCore::getPaymentWidgetContent(
                $paymentWidgetUrl,
                $this->getServerMode()
            );

            if (!$paymentWidgetContent['isValid'] ||
                strpos($paymentWidgetContent['response'], 'errorDetail') !== false) {
                $paymentWidget ['isValid'] = false;
                $paymentWidget ['response'] = 'ERROR_GENERAL_REDIRECT';
            } else {
                $paymentWidget ['isValid'] = true;
                $paymentWidget ['response'] = $paymentWidgetUrl;
            }
        }

        return $paymentWidget ;
    }


    /**
    * provide chart data
    *
    * @param array $cart
    * @return array $cartItems
    */
    public function getCartItems($cart)
    {
        $cartItems = array();
        $i = 0;

        foreach ($cart as $key => $value) {
            if ($value->kArtikel != 0) {
                $cartItems[$i]['merchant_item_id'] = $value->Artikel->cArtNr;
                $cartItems[$i]['quantity'] = (int) $value->nAnzahl;
                $cartItems[$i]['name'] = $value->Artikel->cName;
                $cartItems[$i]['price'] = $value->Artikel->Preise->fVKBrutto;
                $cartItems[$i]['discount'] = $value->Artikel->Preise->rabatt;
                $cartItems[$i]['tax'] = $value->Artikel->Preise->fUst;
                $i = $i + 1;
            }
        }
        return $cartItems;
    }

    /**
    * provide customer data for recurring payment method
    *
    * @return array $transactionData
    */
    public function getCustomerRecurringParameters()
    {
        $transactionData = array();
        $transactionData['customer']['email'] = $_SESSION['Kunde']->cMail;
        $transactionData['customer']['first_name'] = $_SESSION['Kunde']->cVorname;
        $transactionData['customer']['last_name'] = $_SESSION['Kunde']->cNachname;
        $transactionData['billing']['street'] = $_SESSION['Kunde']->cStrasse . ' ' . $_SESSION['Kunde']->cHausnummer;
        $transactionData['billing']['city'] = $_SESSION['Kunde']->cOrt;
        $transactionData['billing']['zip'] = $_SESSION['Kunde']->cPLZ;
        $transactionData['billing']['country_code'] = $_SESSION['Kunde']->cLand;
        $transactionData['amount'] = $this->getRegisterAmount();
        $transactionData['currency'] =  $_SESSION['Waehrung']->cISO;
        $transactionData['customer_ip'] = $this->getCustomerIp();

        return $transactionData;
    }

    /**
    * provide customer data for recurring payment method
    *
    * @param boolean $id
    * @return string $paymentWidgetUrl
    */
    public function getRecurringPaymentWidgetUrl($id = false)
    {
        $transactionData = array_merge_recursive(
            $this->getCredentials(),
            $this->getCustomerRecurringParameters(),
            $this->getCC3DParameters(
                $this->getRegisterAmount(),
                $_SESSION['Waehrung']->cISO
            )
        );

        $transactionData['test_mode'] = $this->getTestMode();
        if ($this->getGroupRecurring() != 'VA') {
            $transactionData['payment_type'] = $this->getPaymentType();
        }
        $transactionData['payment_recurring']    = 'INITIAL';
        $transactionData['payment_registration'] = 'true';

        if ($id) { // change payment
            $transactionData['transaction_id'] = $this->getReferenceId($id);
        } else {
            $transactionData['transaction_id'] = $this->userId;
        }

        $checkoutResult = GenericshopPaymentCore::getCheckoutResult($transactionData);
        if (!$checkoutResult['isValid'] || !isset($checkoutResult['response']['id'])) {
            $paymentWidget ['isValid'] = false;
            $paymentWidget ['response'] = 'ERROR_GENERAL_REDIRECT';
        } else {
            $paymentWidgetUrl = GenericshopPaymentCore::getPaymentWidgetUrl(
                $transactionData,
                $checkoutResult['response']['id']
            );
            $paymentWidgetContent = GenericshopPaymentCore::getPaymentWidgetContent(
                $paymentWidgetUrl,
                $this->getServerMode()
            );

            if (!$paymentWidgetContent['isValid'] ||
                strpos($paymentWidgetContent['response'], 'errorDetail') !== false) {
                $paymentWidget ['isValid'] = false;
                $paymentWidget ['response'] = 'ERROR_GENERAL_REDIRECT';
            } else {
                $paymentWidget ['isValid'] = true;
                $paymentWidget ['response'] = $paymentWidgetUrl;
            }
        }
        
        return $paymentWidget;
    }

    /**
    * prepare payment process
    *
    * @param object $order
    */
    public function preparePaymentProcess($order)
    {
        if ($this->duringCheckout) {
            $this->doStandardCheckout($order);
        } else {
            $this->doGenericshopCheckout($order);
        }
    }

    /**
    * do checkout using standart process from JTL
    *
    * @param object $order
    */
    public function doStandardCheckout($order)
    {
        Jtllog::writeLog('debug log : start checkout process', JTLLOG_LEVEL_ERROR);
        global $oPlugin, $smarty;

        $this->duringCheckout = 1;
        $hash = $this->generateHash($order);
        $paymentWidget = $this->getPaymentWidget($order);
        if (!$paymentWidget['isValid']) {
            $this->redirectError($paymentWidget['response']);
        } else {
            $smarty->assign('lang_code', $this->getLangCode());
            $smarty->assign('testMode', $this->getTestMode());
            $smarty->assign('paymentMethod', $this->paymentMethod);
            $smarty->assign('brand', $this->getBrand());
            $smarty->assign('recurring', $this->isRecurring());
            $smarty->assign('paymentWidgetUrl', $paymentWidget['response']);
            $smarty->assign('registrations', $this->getRegistrations());

            $cancelUrl   = gibShopURL().'/bestellvorgang.php?editZahlungsart=1';
            $responseUrl = $this->getNotificationURL($hash);
            $pluginPath  = gibShopURL()."/".PFAD_PLUGIN.$oPlugin->cVerzeichnis."/"
                .PFAD_PLUGIN_VERSION.$oPlugin->nVersion."/paymentmethod";

            $smarty->assign('responseUrl', $responseUrl);
            $smarty->assign('cancelUrl', $cancelUrl);
            $smarty->assign('pluginPath', $pluginPath);
        }
        

        Jtllog::writeLog('debug log : end checkout process', JTLLOG_LEVEL_ERROR);
    }

    /**
    * do checkout using custom checkout template
    *
    * @param object $order
    */
    public function doGenericshopCheckout($order)
    {
        Jtllog::writeLog('debug log : start checkout process', JTLLOG_LEVEL_ERROR);
        global $oPlugin, $smarty;
        $hash = $this->generateHash($order);
        $paymentWidget = $this->getPaymentWidget($order);
        if (!$paymentWidget['isValid']) {
            $this->redirectError($paymentWidget['response']);
        } else {
            $widgetData['lang_code'] = $this->getLangCode();
            $widgetData['testMode'] = $this->getTestMode();
            $widgetData['paymentMethod'] = $this->paymentMethod;
            $widgetData['brand'] = $this->getBrand();
            $widgetData['recurring'] = $this->isRecurring();
            $widgetData['paymentWidgetUrl'] = $paymentWidget['response'];
            $widgetData['registrations'] = $this->getRegistrations();

            $cancelUrl   = gibShopURL().'/bestellvorgang.php?editZahlungsart=1';
            $responseUrl = $this->getNotificationURL($hash);
            $pluginPath  = gibShopURL()."/".PFAD_PLUGIN.$oPlugin->cVerzeichnis."/"
                .PFAD_PLUGIN_VERSION.$oPlugin->nVersion."/paymentmethod";

            $widgetData['responseUrl'] = $responseUrl;
            $widgetData['cancelUrl'] = $cancelUrl;
            $widgetData['pluginPath'] = $pluginPath;
            $_SESSION['widgetData'] = $widgetData;

            $header = 'Location: ' . gibShopURL() . '/' .$this->getGenericshopCheckoutLink();
            header($header);
            exit();
        }
        

        Jtllog::writeLog('debug log : end checkout process', JTLLOG_LEVEL_ERROR);
    }

    /**
     * return custom checkout page link based on current session language
     *
     * @return string
     */
    public function getGenericshopCheckoutLink()
    {
        $result = Shop::DB()->query(
            "SELECT b.cSeo FROM tlink a
            LEFT JOIN tlinksprache b
            ON (a.kLink = b.kLink)
            WHERE b.cISOSprache = '".$_SESSION['cISOSprache']."'
            AND a.cName = 'genericshop checkout page'",
            1
        );

        return $result->cSeo;
    }

    /**
    * provide random number
    *
    * @return string $result
    */
    public function randomNumber($length)
    {
        $result = '';

        for ($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    /**
    * change order status
    *
    * @param object $order
    */
    public function setOrderStatusToProcessing($order)
    {
        global $DB;

        $query = 'UPDATE tbestellung SET '
            .'    dBezahltDatum = now(), '
            .'    cStatus = "'.BESTELLUNG_STATUS_IN_BEARBEITUNG.'" '
            .'WHERE kBestellung = '.intval($order->kBestellung);
        $DB->executeQuery($query, 4);
    }

    /**
    * @param object $order
    * @param string $paymentHash
    * @param array $args
    */
    public function handleNotification($order, $paymentHash, $args)
    {
        global $DB;

        if (!$this->duringCheckout) {
            $this->finalizeOrder($order, $paymentHash, $args);
        } else {
            $query = "UPDATE xplugin_jtl_genericshop_orders SET order_id = '"
                .intval($order->kBestellung)."' WHERE ref_id = '".$DB->escape($args['ref_id'])."'";
            $DB->executeQuery($query, 4);
        }
            $incomingPayment = new stdClass();
            $incomingPayment->cISO = $order->Waehrung->cISO;
            $incomingPayment->fBetrag = $this->getAmountByOrderWarensummeLocalized($order->WarensummeLocalized);
            $incomingPayment->cZahler = $order->oRechnungsadresse->cMail;
            $incomingPayment->cHinweis = $args['ref_id'];
            $this->addIncomingPayment($order, $incomingPayment);

        if ($this->getPaymentType() == 'PA') {
            $this->setOrderStatusToProcessing($order);
        } else {
            $this->setOrderStatusToPaid($order);
        }
            $this->sendConfirmationMail($order);

            $header = 'Location: '.gibShopURL().'/bestellabschluss.php?i='.$paymentHash.
            '&isDuringCheckout='.$this->duringCheckout;
            header($header);
            exit();
    }

    /**
    * insert transaction order to database
    *
    * @param object $order
    * @param array $resultJson
    */
    public function addGenericshopOrder($order, $resultJson)
    {
        global $DB;

        $paymentName = $order->cZahlungsartName;
        if ($this->paymentMethod == "genericshop_cc" || $this->paymentMethod == "genericshop_ccsaved") {
            if ($resultJson['paymentType'] == 'PA') {
                $paymentName = $paymentName." (Pre-Authorization)";
            } else {
                $paymentName = $paymentName." (Debit)";
            }
        }

        if (GenericshopPaymentCore::isSuccessReview($resultJson['result']['code'])) {
            $status = $this->getTranslate('BACKEND_BT_REVIEW');
        } elseif ($resultJson['paymentType'] == 'PA') {
            $status = $this->getTranslate('BACKEND_TT_PA');
        } else {
            $status = $this->getTranslate('BACKEND_TT_ACC');
        }

        $query = "INSERT INTO xplugin_jtl_genericshop_orders (
                    transaction_id,
                    order_id,
                    ref_id,
                    payment_method,
                    payment_name,
                    amount, currency,
                    payment_type, status
                )  VALUES ('".
                    $resultJson['merchantTransactionId']."',".
                    (isset($order->kBestellung)?intval($order->kBestellung):0).",'".
                    $resultJson['id']."','".
                    $this->paymentMethod."','".
                    $paymentName."','".
                    $resultJson['amount']."','".
                    $resultJson['currency']."','".
                    $resultJson['paymentType']."','".
                    $status.
                "')";
        $DB->executeQuery($query, 3);
    }

    /**
    * delete payment account that already registered
    *
    * @param string $registrationId
    * @param array GenericshopPaymentCore::deleteRegistration($registrationId, $transactionData)
    */
    public function doDeRegistration($registrationId)
    {
        $transactionData = $this->getCredentials();
        $transactionData['test_mode'] = $this->getTestMode();

        return GenericshopPaymentCore::deleteRegistration($registrationId, $transactionData);
    }


    /**
    * process sucess payment
    *
    * @param array $resultJson
    * @param object $order
    * @return boolean
    */
    public function processPaymentSuccess($paymentResult, $order)
    {
        Jtllog::writeLog('debug log : proces succes payment', JTLLOG_LEVEL_ERROR);
        $registrationId = $paymentResult['registrationId'];
        Jtllog::writeLog('debug log : registration id :'.$registrationId, JTLLOG_LEVEL_ERROR);

        if ($this->isRecurring()) {
            $this->insertRegistration($registrationId, $paymentResult);
        }

        $_REQUEST['ref_id'] = $paymentResult['id'];
        Jtllog::writeLog('debug log : ref id : '.$_REQUEST['ref_id'], JTLLOG_LEVEL_ERROR);

        $this->addGenericshopOrder($order, $paymentResult);
        return true;
    }

    /**
    * finalize order process
    *
    * @param object $order
    * @param string $hash
    * @param array $args
    * @return boolean
    */
    public function finalizeOrder($order, $hash, $args)
    {
        $this->createObjectOplugin();
        VersionTracker::sendVersionTracker($this->getVersionData());
        $checkoutId = isset($args['id']) ? $args['id'] : '';
        $registrationId = isset($args['registrationId']) ? $args['registrationId'] : '';
        $credentials = $this->getCredentials();
        $paymentResult = GenericshopPaymentCore::getPaymentResult($checkoutId, $credentials);

        if (!$paymentResult['isValid']) {
            $this->redirectError($paymentResult['response']);
        } else {
            $returnCode = $paymentResult['response']["result"]["code"];
            $returnMessage = GenericshopPaymentCore::getErrorIdentifier($returnCode);
            $transactionResult = GenericshopPaymentCore::getTransactionResultCode($returnCode);

            if ($transactionResult == "ACK") {
                return $this->processPaymentSuccess($paymentResult['response'], $order);
            } elseif ($transactionResult == "NOK") {
                $this->redirectError($returnMessage);
            } else {
                $this->redirectError('ERROR_UNKNOWN');
            }
        }
    }

    /**
     * create global variable $Plugin if  those  global variable already unset in previous process
     *
     * @return string
     */
    protected function createObjectOplugin()
    {
        global $oPlugin;
        if (empty($oPlugin)) {
            $kPlugin = gibkPluginAuscModulId($_SESSION['Zahlungsart']->cModulId);
            if ($kPlugin > 0) {
                $oPlugin            = new Plugin($kPlugin);
                $GLOBALS['oPlugin'] = $oPlugin;
            }
        }
    }

    /**
    * @param array $referenceId
    * @param string $recurringId
    * @param array $paymentResult
    * @param array $transactionData
    * @param array $registrationId
    */
    public function refundRecurringPayment(
        $referenceId,
        $recurringId,
        $paymentResult,
        $transactionData,
        $registrationId
    ) {

        $transactionData['payment_type'] = 'RF';
        GenericshopPaymentCore::backOfficeOperation($referenceId, $transactionData);
        $this->saveRegistration($recurringId, $registrationId, $paymentResult);
    }

    /**
    * @param string $recurringId
    * @param array $paymentResult
    * @param array $transactionData
    * @return string
    */
    public function processPaymentSavedRecurring($recurringId, $paymentResult, $transactionData)
    {
        $referenceId = $paymentResult['id'];
        $registrationId = $paymentResult['registrationId'];
        if ($this->getPaymentType() == 'PA') {
            $transactionData['payment_type'] = "CP";
            $response = GenericshopPaymentCore::backOfficeOperation($referenceId, $transactionData);
            if ($response['isValid']) {
                $returnCode = $response['response']["result"]["code"];
                $returnMessage = GenericshopPaymentCore::getErrorIdentifier($returnCode);
                $resultCP = GenericshopPaymentCore::getTransactionResultCode($returnCode);

                if ($resultCP == "ACK") {
                    $referenceId = $response['response']['id'];
                } else {
                    $transactionData['payment_type'] = "RV";
                    GenericshopPaymentCore::backOfficeOperation($referenceId, $transactionData);
                    GenericshopPaymentCore::deleteRegistration($registrationId, $transactionData);
                    return $returnMessage;
                }
            } else {
                return $response['response'];
            }
        }

        $this->refundRecurringPayment(
            $referenceId,
            $recurringId,
            $paymentResult,
            $transactionData,
            $registrationId
        );
        return 'success';
    }

    /**
    * @param string $recurringId
    * @param array $paymentResult
    * @param array $transactionData
    * @return string
    */
    public function processPaymentSuccessRecurring($recurringId, $paymentResult, $transactionData)
    {
        $transactionData['amount'] = $this->getRegisterAmount();
        $transactionData['currency'] = $_SESSION['Waehrung']->cISO;
        if ($recurringId) {
            $transactionData['transaction_id'] = $paymentResult['merchantTransactionId'];
        } else {
            $transactionData['transaction_id'] = $this->userId;
        }

        $transactionData['payment_recurring'] = 'INITIAL';
        $transactionData['test_mode'] = $this->getTestMode();
        $result = $this->processPaymentSavedRecurring($recurringId, $paymentResult, $transactionData);
        
        if ($recurringId && $result=='success') {
            $referenceId = $paymentResult['merchantTransactionId'];
            GenericshopPaymentCore::deleteRegistration($referenceId, $transactionData);
        }
        return $result;
    }

    /**
    * @param string $checkoutId
    * @param string $recurringId
    * @return string
    */
    public function finalizeRecurring($checkoutId, $recurringId)
    {
        VersionTracker::sendVersionTracker($this->getVersionData());
        $transactionData = $this->getCredentials();
        $paymentResult = GenericshopPaymentCore::getPaymentResult($checkoutId, $transactionData);

        if (!$paymentResult['isValid']) {
            return $paymentResult['response'];
        } else {
            $returnCode = $paymentResult['response']["result"]["code"];
            $returnMessage = GenericshopPaymentCore::getErrorIdentifier($returnCode);
            $transactionResult = GenericshopPaymentCore::getTransactionResultCode($returnCode);

            if ($transactionResult == "ACK") {
                return $this->processPaymentSuccessRecurring(
                    $recurringId,
                    $paymentResult['response'],
                    $transactionData
                );
            } elseif ($transactionResult == "NOK") {
                return $returnMessage;
            } else {
                return 'ERROR_UNKNOWN';
            }
        }
    }

    /**
    * @param string $id
    * @return string
    */
    public function deleteRecurring($id)
    {
        $transactionData = $this->getCredentials();
        $transactionData['test_mode'] = $this->getTestMode();
        $referenceId = $this->getReferenceId($id);

        $resultJson = GenericshopPaymentCore::deleteRegistration($referenceId, $transactionData);
        if (!$resultJson['isValid']) {
            return $resultJson['response'];
        } else {
            $returnCode = $resultJson['response']["result"]["code"];
            $returnMessage = GenericshopPaymentCore::getErrorIdentifier($returnCode);
            $result = GenericshopPaymentCore::getTransactionResultCode($returnCode);

            if ($result == "ACK") {
                $this->deleteRegistration($id);
                return 'success';
            } elseif ($result == "NOK") {
                return $returnMessage;
            }
        }
    }

    /**
    * @param string $returnMessage
    */
    public function redirectError($returnMessage)
    {
        
        $header = 'Location: '.gibShopURL().'/bestellvorgang.php?editZahlungsart=1&error='.$returnMessage;
        header($header);
        exit();
    }

    /**
    * @param string $cOrderId
    * @param string $paymentType
    * @return boolean
    */
    public function doBackendPayment($cOrderId, $paymentType)
    {
        global $DB;

        $payment = $DB->executeQuery(
            "SELECT * from xplugin_jtl_genericshop_orders WHERE order_id = '"
            .intval($cOrderId)."'",
            1
        );

        $transactionData = $this->getCredentials();
        if ($this->isMultiChannel()) {
            $transactionData['channel_id'] = $transactionData['channel_id_moto'];
        }
        $transactionData['currency'] = $payment->currency;
        $transactionData['amount'] = $payment->amount;
        $transactionData['test_mode'] = $this->getTestMode();
        $transactionData['payment_type'] = $paymentType;
        $response = GenericshopPaymentCore::backOfficeOperation($payment->ref_id, $transactionData);
       
        Jtllog::writeLog(
            'debug log : response from gateway : '.json_encode($response), JTLLOG_LEVEL_ERROR
        );

        if (!$response['isValid']) {
            return false;
        } else {
            $returnCode        = $response['response']["result"]["code"];
            $transactionResult = GenericshopPaymentCore::getTransactionResultCode($returnCode);
            Jtllog::writeLog(
                'debug log : response from gateway : '.json_encode($transactionResult), JTLLOG_LEVEL_ERROR
             );
            if ($transactionResult == 'ACK') {
                if ($paymentType == 'CP') {
                    $status = 'Zahlung akezptiert';
                    $query    = "UPDATE tbestellung SET cStatus = '".BESTELLUNG_STATUS_BEZAHLT
                        ."' WHERE kBestellung = '".intval($cOrderId)."'";
                    $DB->executeQuery($query, 4);
                    $this->sendMail($cOrderId, MAILTEMPLATE_BESTELLUNG_BEZAHLT);
                }
                if ($paymentType == 'RF') {
                    $status = 'Refunded';
                }

                $query = "UPDATE xplugin_jtl_genericshop_orders SET status = '".$status."', payment_type = '"
                    .$paymentType."' WHERE order_id = '".intval($cOrderId)."'";
                $DB->executeQuery($query, 4);

                return true;
            }
        }
        return false;
    }

    /**
    * @param string $cOrderId
    * @return boolean
    */
    public function doUpdateStatus($cOrderId)
    {
        global $DB;

        $payment = $DB->executeQuery(
            "SELECT ref_id from xplugin_jtl_genericshop_orders WHERE order_id = '"
            .intval($cOrderId)."'",
            1
        );

        $refId = $payment->ref_id;
        $transactionData = $this->getCredentials();
        $transactionData['test_mode'] = $this->getTestMode();
        $result = GenericshopPaymentCore::updateStatus($refId, $transactionData);

        if ($result['isValid']) {
            $xmlResult = simplexml_load_string($result['response']);

            if (isset($xmlResult->Result)) {
                $returnCode = (string) $xmlResult->Result->Transaction->Processing->Return['code'];
                $resultStatus = GenericshopPaymentCore::getTransactionResultCode($returnCode);

                if ($resultStatus == 'ACK') {
                    if (GenericshopPaymentCore::isSuccessReview($returnCode)) {
                        return true;
                    }

                    $paymentCode = (string) $xmlResult->Result->Transaction->Payment['code'];
                    $paymentType = substr($paymentCode, -2);

                    if ($paymentType == 'PA') {
                        $status = $this->getTranslate('BACKEND_TT_PA');
                    }
                    if ($paymentType == 'DB') {
                        $status = $this->getTranslate('BACKEND_TT_ACC');
                        $query    = "UPDATE tbestellung SET cStatus = '".BESTELLUNG_STATUS_BEZAHLT
                            ."' WHERE kBestellung = '".intval($cOrderId)."'";
                        $DB->executeQuery($query, 4);
                        $this->sendMail($cOrderId, MAILTEMPLATE_BESTELLUNG_BEZAHLT);
                    }

                    $query = "UPDATE xplugin_jtl_genericshop_orders SET status = '".$status
                        ."', payment_type = '".$paymentType."' WHERE order_id = '".intval($cOrderId)."'";
                    $DB->executeQuery($query, 4);

                    return true;
                }
            }
        } else {
            return false;
        }
    }

     /**
    * @param string $identifier
    * @return string
    */
    public function getTranslate($identifier)
    {
        global $oPlugin;

        return $oPlugin->oPluginSprachvariableAssoc_arr[$identifier];
    }

    /**
    * get language iso code
    *
    * @return string $lang_code
    */
    public function getLangCode()
    {
        if (convertISO2ISO639($_SESSION['cISOSprache']) == "de") {
            $langCode = "de";
        } else {
            $langCode = "en";
        }

        return $langCode;
    }

    /**
    * get card brand
    *
    * @return string
    */
    public function getBrand()
    {
        switch ($this->paymentMethod) {
            case 'genericshop_cc':
            case 'genericshop_ccsaved':
                return $this->getBrandCard();
                break;
            default:
                return strtoupper(str_replace('genericshop_', '', $this->paymentMethod));
                break;
        }
    }

    /**
    * get credit card brand
    *
    * @return string
    */
    public function getBrandCard()
    {
        $brand = '';
        if ($this->getConfig('visa')) {
            $brand .= 'VISA ';
        }
        if ($this->getConfig('master')) {
            $brand .= 'MASTER ';
        }
        if ($this->getConfig('amex')) {
            $brand .= 'AMEX ';
        }
        if ($this->getConfig('diners')) {
            $brand .= 'DINERS ';
        }
        if ($this->getConfig('jcb')) {
            $brand .= 'JCB ';
        }

        return trim($brand);
    }

    /**
    * get server config value
    *
    * @return string
    */
    public function getServerMode()
    {
        return $this->getConfig('server');
    }

    /**
    * get credit card brand
    *
    * @return array $credentials
    */
    public function getCredentials()
    {
        $credentials = array(
            'server_mode' => $this->getServerMode(),
            'channel_id' => $this->getConfig('entity_id'),
            'login' => $this->getConfigGeneral('userid'),
            'password' => $this->getConfigGeneral('password')
        );

        if ($this->isMultiChannel()) {
            $credentials['channel_id_moto'] = $this->getConfig('entity_id_moto');
        }

        return $credentials;
    }

    /**
    * is multichanel
    *
    * @return string
    */
    public function isMultiChannel()
    {
        return $this->getConfig('multichannel');
    }

    /**
    * get registered amount configs value
    *
    * @return string
    */
    public function getRegisterAmount()
    {
        return $this->getConfig('amount');
    }

    /**
    * get server configs value
    *
    * @return string
    */
    public function getPaymentTypeSelection()
    {
        return $this->getConfig('transaction_mode');
    }

    /**
    * get payment type
    *
    * @return string
    */
    public function getPaymentType()
    {
        switch ($this->paymentMethod) {
            case 'genericshop_cc':
            case 'genericshop_ccsaved':
                return $this->getPaymentTypeSelection();
                break;
            default:
                return 'DB';
                break;
        }
    }

    /**
    * is test mode
    *
    * @return string
    */
    public function getTestMode()
    {
        if ($this->getServerMode() == "LIVE") {
            return false;
        }
        return $this->testMode;
    }

    /**
    * is recurring configs active
    *
    * @return string
    */
    public function isRecurringActive()
    {
        return $this->getConfigGeneral('recurring');
    }

    /**
    * is recurring
    *
    * @return boolean
    */
    public function isRecurring()
    {
        switch ($this->paymentMethod) {
            case 'genericshop_ccsaved':
                return true;
                break;
            default:
                return false;
                break;
        }
    }

    /**
    * recurring payment grouping
    *
    * @return string
    */
    public function getGroupRecurring()
    {
        switch ($this->paymentMethod) {
            case 'genericshop_ccsaved':
                return 'CC';
                break;
        }
    }

    /**
    * provide user ip
    *
    * @return string
    */
    public function getCustomerIp()
    {
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            return '127.0.0.1';
        }
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
    * provide merchant email
    *
    * @return string
    */
    public function getMerchantEmail()
    {
        global $DB;

        $merchantEmail = $this->getConfigGeneral('merchantemail');
        if ($merchantEmail) {
            return $merchantEmail;
        }

        $email = $DB->executeQuery(
            "SELECT cMail from tadminlogin where kAdminlogingruppe = 1 and bAktiv = 1",
            1
        );
        return $email->cMail;
    }

    /**
    * provide merchant merchant data
    *
    * @return array
    */
    public function getMerchantData()
    {
        return array(
            'merchant_email' => $this->getMerchantEmail(),
            'merchant_no' => $this->getConfigGeneral('merchantno'),
            'shop_url' => $this->getConfigGeneral('shopurl'),
            'merchant_location' => $this->getConfigGeneral('merchant_location')
        );
    }

    /**
    * Get version tracker parameters
    *
    * @return array
    */
    public function getVersionData()
    {
        return array_merge(
            $this->getGeneralVersionData(),
            $this->getCreditCardVersionData()
        );
    }

    /**
    * Get general version tracker parameters
    *
    * @return array
    */
    protected function getGeneralVersionData()
    {
        global $oPlugin;

        $merchant = $this->getMerchantData();

        $versionData['transaction_mode'] = $this->getServerMode();
        $versionData['ip_address'] = $_SERVER['SERVER_ADDR'];
        $versionData['shop_version'] = JTL_VERSION;
        $versionData['plugin_version'] = $oPlugin->nVersion;
        $versionData['client'] = $this->client;
        $versionData['email'] = $merchant['merchant_email'];
        $versionData['merchant_id'] = $merchant['merchant_no'];
        $versionData['shop_system'] = $this->shopSystem;
        $versionData['shop_url'] = $merchant['shop_url'];

        return $versionData;
    }

    /**
    * Get credit card version tracker parameters
    *
    * @return array
    */
    protected function getCreditCardVersionData()
    {
        $versionData = array();

        if ($this->paymentMethod == 'genericshop_cc' || $this->paymentMethod == 'genericshop_ccsaved') {
            $versionData['merchant_location'] = $this->getConfigGeneral('merchant_location');
        }

        return $versionData;
    }

    /**
    * provide account type
    *
    * @return string
    */
    public function getAccountType()
    {
        switch ($this->paymentMethod) {
            case 'genericshop_ccsaved':
                return 'card';
                break;
        }
    }

    /**
    * provide payment account data
    *
    * @return array $account
    */
    public function getAccount($resultJson)
    {
        $account = $resultJson[$this->getAccountType()];
        $account['email'] = '';
        return $account;
    }

    /**
    * is payment account use as default payment
    *
    * @return boolean
    */
    public function checkDefault()
    {
        global $DB;

        $credentials = $this->getCredentials();
        $data        = $DB->executeQuery(
            "SELECT * from xplugin_jtl_genericshop_recurring where cust_id='"
            .$this->userId."' and server_mode='".$credentials['server_mode']
            ."' and channel_id='".$credentials['channel_id']."' and payment_group='"
            .$this->getGroupRecurring()."' and payment_default = 1",
            3
        );
        if ($data > 0) {
            return false;
        }
        return true;
    }

    /**
    * is payment account already registered
    *
    * @return boolean
    */
    public function isRegistered($registrationId)
    {
        global $DB;

        $data = $DB->executeQuery(
            "SELECT ref_id from xplugin_jtl_genericshop_recurring where ref_id='"
            .$registrationId."' and cust_id='".$this->userId."' and payment_group='"
            .$this->getGroupRecurring()."'",
            3
        );

        if ($data > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * get refrence id
    *
    * @return string $data->ref_id
    */
    public function getReferenceId($id)
    {
        global $DB;

        $data = $DB->executeQuery(
            "SELECT ref_id from xplugin_jtl_genericshop_recurring where id='"
            .(int) $id."' and cust_id='".$this->userId."' and payment_group='"
            .$this->getGroupRecurring()."'",
            1
        );
        return $data->ref_id;
    }

    /**
    * get regitered payment account
    *
    * @return object $data
    */
    public function getRegistrations()
    {
        global $DB;

        $credentials = $this->getCredentials();
        $data = $DB->executeQuery(
            "SELECT * from xplugin_jtl_genericshop_recurring where cust_id='"
            .$this->userId."' and server_mode='".$credentials['server_mode']
            ."' and channel_id='".$credentials['channel_id']."' and payment_group='"
            .$this->getGroupRecurring()."'",
            9
        );
        return $data;
    }

    /**
    * change default payment accout
    */
    public function updateDefaultRegistration($id)
    {
        global $DB;

        $DB->executeQuery(
            "UPDATE xplugin_jtl_genericshop_recurring SET payment_default = '0' "
            ."WHERE payment_default = '1' AND payment_group ='"
            .$this->getGroupRecurring()."'",
            4
        );
        $DB->executeQuery(
            "UPDATE xplugin_jtl_genericshop_recurring SET payment_default = '1' "
            ."WHERE id = '".(int) $id."' AND payment_group ='"
            .$this->getGroupRecurring()."'",
            4
        );
    }

    /**
    * registering payment account
    */
    public function insertRegistration($registrationId, $resultJson)
    {
        global $DB;

        $isRegistered = $this->isRegistered($registrationId);
        if (!$isRegistered) {
            $credentials = $this->getCredentials();
            $default = $this->checkDefault();
            $account = $this->getAccount($resultJson);
            $DB->executeQuery(
                "INSERT INTO xplugin_jtl_genericshop_recurring (cust_id, payment_group,"
                . " brand, holder, email, last4digits, expiry_month, expiry_year, server_mode,"
                . " channel_id, ref_id, payment_default) VALUES ('".$this->userId."', '"
                .$this->getGroupRecurring()."', '".$resultJson['paymentBrand']."', '"
                .$account['holder']."', '".$account['email']."', '".$account['last4Digits']
                ."', '".$account['expiryMonth']."', '".$account['expiryYear']."', '"
                .$credentials['server_mode']."', '".$credentials['channel_id']."', '"
                .$registrationId."', '".$default."')",
                4
            );
        }
    }

     /**
    * change payment account that already registered
    *
    * @param string $id
    * @param string $registrationId
    * @return array $resultJson
    */
    public function updateRegistration($id, $registrationId, $resultJson)
    {
        global $DB;

        $credentials = $this->getCredentials();
        $account = $this->getAccount($resultJson);
        $DB->executeQuery(
            "UPDATE xplugin_jtl_genericshop_recurring SET cust_id = '".$this->userId
            ."', payment_group = '".$this->getGroupRecurring()."', brand = '"
            .$resultJson['paymentBrand']."', holder = '".$account['holder']."', email = '"
            .$account['email']."', last4digits = '".$account['last4Digits']."', expiry_month = '"
            .$account['expiryMonth']."', expiry_year = '".$account['expiryYear']."', server_mode = '"
            .$credentials['server_mode']."', channel_id = '".$credentials['channel_id']
            ."', ref_id = '".$registrationId."' WHERE id = '".(int)$id."'",
            4
        );
    }
    /**
    * change or add new payment account
    *
    * @param string $id
    * @param string $registrationId
    * @return array $resultJson
    */
    public function saveRegistration($id, $registrationId, $resultJson)
    {
        if ($id) {
            $this->updateRegistration($id, $registrationId, $resultJson);
        } else {
            $this->insertRegistration($registrationId, $resultJson);
        }
    }

    /**
    * delete payment account that already registered
    *
    * @param string $id
    */
    public function deleteRegistration($id)
    {
        global $DB;

        $DB->executeQuery(
            "DELETE FROM xplugin_jtl_genericshop_recurring where id='".
            $id."' and cust_id='".$this->userId."' and payment_group='"
            .$this->getGroupRecurring()."'",
            4
        );
    }

    /**
    * get customer sex
    *
    * @param string $salutation
    * @return string
    */
    public static function getCustomerSex($salutation)
    {
        switch ($salutation) {
            case 'm':
                return 'M';
                break;
            case 'w':
                return 'F';
                break;
            default:
                return '';
                break;
        }
    }

    /**
    * get customer birth date
    *
    * @return string $birthday
    */
    public static function getCustomerBirthday()
    {
        if (isset($_SESSION['Kunde']->dGeburtstag) && $_SESSION['Kunde']->dGeburtstag
            != '00.00.0000') {
            $date     = $_SESSION['Kunde']->dGeburtstag;
            $birthday = date("Y-m-d", strtotime($date));

            return $birthday;
        }

        return '';
    }

    /**
    * set currency format
    *
    * @return string
    */
    public function setNumberFormat($number)
    {
        return number_format(str_replace(',', '.', $number), 2, '.', '');
    }

    /**
     * is valid date of birth
     *
     * @return boolean
     */
    public static function isDateOfBirthValid()
    {
        $dateOfBirth = explode("-", self::getCustomerBirthday());
        $today = explode("-", date('Y-m-d'));

        $year = (int)$dateOfBirth[0];
        $month = (int)$dateOfBirth[1];
        $day = (int)$dateOfBirth[2];

        $thisYear = (int)$today[0];

        if ($year < 1900) {
            return false;
        }

        if ($month < 0 || $month > 12) {
            return false;
        }

        if ($day < 0 || $day > 31) {
            return false;
        }

        $valid = checkdate($month, $day, $year);

        if (!$valid) {
            return false;
        }
        return true;
    }

    /**
     * is date of birth lower than today
     *
     * @return boolean
     */
    public static function isDateOfBirthLowerThanToday()
    {
        $dateOfBirth = strtotime(self::getCustomerBirthday());
        $today = strtotime(date('Y-m-d'));

        if ($dateOfBirth < $today) {
            return true;
        }
        return false;
    }


    /**
     * is total amount allowed
     *
     * @return boolean
     */
    public static function isAmountAllowed()
    {
        $totalAmount = self::getTotalAmount();
        $isCurrencyEuro = self::isCurrencyEuro();

        if ($isCurrencyEuro && $totalAmount >=200 && $totalAmount <=5000) {
            return true;
        }
        return false;
    }

    /**
     * Determines if currency euro.
     *
     * @return     boolean  True if currency euro, False otherwise.
     */
    public static function isCurrencyEuro()
    {
        $currency = $_SESSION['cWaehrungName'];
        if ($currency != 'EUR') {
            return false;
        }
        return true;
    }

    /**
     * Gets the total amount.
     *
     * @return     integer  The total amount.
     */
    public static function getTotalAmount()
    {
        $totalAmount = 0;
        foreach ($_SESSION['Warenkorb']->PositionenArr as $amount) {
            if (isset($amount->cGesamtpreisLocalized['0']['EUR'])) {
                $euroAmount = explode(' ', $amount->cGesamtpreisLocalized['0']['EUR']);
                $amountValue = str_replace('.', '', $euroAmount[0]);
                $amountValue = str_replace(',', '.', $amountValue);

                $totalAmount += (float)$amountValue;
            }
        }
        return $totalAmount;
    }

    /**
     * is billing equal shipping
     *
     * @return boolean
     */
    public static function isBillingEqualShipping()
    {
        $billingAddresses = $_SESSION['Kunde']->cStrasse." ".
                             $_SESSION['Kunde']->cHausnummer." ".
                             $_SESSION['Kunde']->cOrt." ".
                             $_SESSION['Kunde']->cPLZ." ".
                             $_SESSION['Kunde']->cLand;

        $shippingAddresses = $_SESSION['Lieferadresse']->cStrasse." ".
                             $_SESSION['Lieferadresse']->cHausnummer." ".
                             $_SESSION['Lieferadresse']->cOrt." ".
                             $_SESSION['Lieferadresse']->cPLZ." ".
                             $_SESSION['Lieferadresse']->cLand;

        if ($billingAddresses == $shippingAddresses) {
            return true;
        }
        return false;
    }

    /**
     * return amount from session that have same format with amount from gateway
     *
     * @return string
     */
    public static function convertAmountFromSession()
    {
        $sesionAmount = $_SESSION['Warenkorb']->gibGesamtsummeWarenLocalized();
        $amount = explode(' ', $sesionAmount[0]);
        $amount = str_replace('.', '', $amount[0]);
        $amount = str_replace(',', '.', $amount);
        return $amount;
    }

    /**
     * returns url where currently opened
     *
     * @return string
     */
    public static function getCurrentUrl()
    {
        $currentUrl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        return $currentUrl;
    }

    /**
     * validate payment risk score
     *
     * @param array $paymentResponse
     * @return boolean
     */
    protected static function isRiskPayment($paymentResponse)
    {
        if (isset($paymentResponse['risk']['score'])) {
            if ((int)$paymentResponse['risk']['score'] < 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * explode string with multi delimiter
     *
     * @param array $delimiters
     * @param string $string
     * @return array $explodedString
     */
    protected static function explodeByMultiDelimiter($delimiters, $string)
    {
        $string = str_replace($delimiters, $delimiters[0], $string);
        $explodedString = explode($delimiters[0], $string);
        return $explodedString;
    }
}
