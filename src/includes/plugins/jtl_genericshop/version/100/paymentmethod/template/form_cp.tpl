<link href="{$pluginPath}/css/payment_form.css" rel="stylesheet" type="text/css">

{if $redirect}
	{literal}
		<style>
			body {
				display: none;
				background: white !important;
			}
		</style>
		<script type="text/javascript">
			var wpwlOptions = {
				onReady: function(){
					jQuery(".wpwl-form").submit();
				}
		    }
		</script>
	{/literal}
{else}
	{literal}
	<script type="text/javascript">
		function decode_utf8(s) {
		  return decodeURIComponent(escape(s));
		}

		var expiryDate = decode_utf8("Gültig bis");
		var cardNumberError = decode_utf8("Ungültige Kartennummer");
		var expiryMonthError = decode_utf8("Ungültiges Ablaufdatum");
		var expiryYearError = decode_utf8("Ungültiges Ablaufdatum");
		var cvvError = decode_utf8("Ungültige Kartenprüfnummer");
		var accountNumberIbanError = decode_utf8("Ungültige IBAN oder Kontonummer");
		var accountBankBicError = decode_utf8("Ungültige BIC (SWIFT-Code) oder Bankleitzahl");
		var accountHolderError = decode_utf8("Ungültiger Kontoinhaber");
		var accountBicError = decode_utf8("Ungültige BIC (SWIFT-Code)");
		var generalTermsAndConditionsError = decode_utf8("Bitte akzeptieren Sie die Einwilligung zur Datenübermittlung");

		var buttonCancel = "<a href='{/literal}{$cancelUrl}{literal}' class='wpwl-button btn_cancel'>{/literal}{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_BT_CANCEL}{literal}</a>";
		var ttTestMode = "<div class='testmode'>{/literal}{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_TT_TESTMODE}{literal}";
		var headerWidget = "<h2 style='text-align : center !important'>{/literal}{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_RECURRING_WIDGET_HEADER2}{literal}</h2>";
		var merchantLocation = "<div class='merchant-location-description'>{/literal}{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_MERCHANT_LOCATION_DESC}{$oPlugin->oPluginEinstellungAssoc_arr['genericshop_general_merchant_location']}{literal}";


		var wpwlOptions = {
			locale: "{/literal}{$lang_code}{literal}",
			{/literal}{if $lang_code == 'de'}{literal}
				labels: {expiryDate: expiryDate},
				errorMessages: {
					cardNumberError: cardNumberError,
					expiryMonthError: expiryMonthError,
					expiryYearError: expiryYearError,
					cvvError: cvvError,
					accountIbanError: accountNumberIbanError,
					accountHolderError : accountHolderError,
					accountBankBicError: accountBankBicError,
					accountBicError: accountBicError,
					generalTermsAndConditionsError: generalTermsAndConditionsError
				},
			{/literal}{/if}{literal}
			style: "card",
			onReady: function(){
	            {/literal}{literal}
	            jQuery('form.wpwl-form').find('.wpwl-button').before(buttonCancel);
	            {/literal}{if $oPlugin->oPluginEinstellungAssoc_arr['genericshop_general_merchant_location'] && ($paymentMethod == 'genericshop_cc' || $paymentMethod == 'genericshop_ccsaved')}{literal}
					jQuery('.wpwl-container').after(merchantLocation);
				{/literal}{/if}{literal}
				jQuery(".wpwl-container").wrap( "<div class='frame'>");
	            {/literal}{if $testMode}{literal}
		            jQuery(".wpwl-container").wrap( "<div class='frametest'>").before(ttTestMode);
	            {/literal}{/if}{literal}
				{/literal}{if $recurring}{literal}
            		jQuery('#wpwl-registrations').after(headerWidget);
            		if (jQuery('.merchant-location-description').length > 1){
            			jQuery('.merchant-location-description').eq(0).hide();
            		}
             	{/literal}{/if}{literal}
			},
			registrations: {
            	hideInitialPaymentForms: false,
            	requireCvv: false
            }
	    }
	</script>
	{/literal}
	{if $recurring}
		{if !empty($registrations)}
			<h2 style="text-align : center !important;">{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_RECURRING_WIDGET_HEADER1}</h2>
		{else}
			<h2 style="text-align : center !important;">{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_MC_PAYANDSAFE}</h2>
		{/if}
		{if !empty($error_message)}
			<p class="box_error">{$error_message}</p>
		{/if}
	{/if}
{/if}
<input type="submit" value="Submit" style="display:none" />
{literal}
<script>
    jQuery(window).load(function() {
	    var jsElement = document.createElement("script");
	    jsElement.type = "text/javascript";
	    jsElement.src = "{/literal}{$paymentWidgetUrl}{literal}";
	    document.head.appendChild(jsElement);
   });
</script>
{/literal}
<form action="{$responseUrl}" class="paymentWidgets">{$brand}</form>
