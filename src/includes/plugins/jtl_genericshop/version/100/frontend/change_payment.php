<?php

include_once(PFAD_ROOT . PFAD_INCLUDES_MODULES . 'PaymentMethod.class.php');

global $oPlugin,$smarty,$DB;

$oPlugin = Plugin::getPluginById('jtl_genericshop');
$id = isset($_REQUEST['id']) ? filterXSS($_REQUEST['id']) : '';
$paymentName = isset($_REQUEST['selected_payment']) ? filterXSS($_REQUEST['selected_payment']) : '';
$payment = PaymentMethod::create('kPlugin_'.$oPlugin->kPlugin.'_'.$paymentName);
$paymentWidgetUrl = $payment->getRecurringPaymentWidgetUrl($id);
$tLink = $DB->executeQuery("SELECT kLink FROM tlink WHERE kPlugin = '"
        .$oPlugin->kPlugin."' AND cName = 'My Payment Information'", 1);
$paymentInfoLink = $tLink->kLink;

if (!$paymentWidgetUrl['isValid']) {
    $header = 'Location: '.gibShopURL().'/index.php?s='
        .$paymentInfoLink.'&sError='.$paymentWidgetUrl['response'];
    header($header);
    exit();
} else {
    $pluginPath = gibShopURL() . "/" . PFAD_PLUGIN . $oPlugin->cVerzeichnis
                  . "/" . PFAD_PLUGIN_VERSION . $oPlugin->nVersion . "/paymentmethod";
    $sError = isset($_GET['sError']) ? filterXSS($_GET['sError']) : '';

    if ($sError) {
        $smarty->assign('sError', $oPlugin->oPluginSprachvariableAssoc_arr[$sError]);
    }

    $smarty->assign('paymentInfoLink', $paymentInfoLink);
    $smarty->assign('pluginPath', $pluginPath);
    $smarty->assign('recurringId', $id);
    $smarty->assign('paymentName', $paymentName);
    $smarty->assign('lang_code', $payment->getLangCode());
    $smarty->assign('testMode', $payment->getTestMode());
    $smarty->assign('brand', $payment->getBrand());
    $smarty->assign('paymentWidgetUrl', $paymentWidgetUrl['response']);
    $smarty->assign('gibShopUrl', gibShopURL());
}
