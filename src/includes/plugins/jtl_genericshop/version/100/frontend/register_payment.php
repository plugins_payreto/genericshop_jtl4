<?php

include_once(PFAD_ROOT . PFAD_INCLUDES_MODULES . 'PaymentMethod.class.php');

global $oPlugin,$smarty,$DB;

$oPlugin = Plugin::getPluginById('jtl_genericshop');
$tLink = $DB->executeQuery(
    "SELECT kLink FROM tlink WHERE kPlugin = '".$oPlugin->kPlugin."' AND cName = 'My Payment Information'",
    1
);
$paymentInfoLink = $tLink->kLink;
$paymentName = isset($_REQUEST['selected_payment']) ? filterXSS($_REQUEST['selected_payment']) : '';

if (empty($paymentName)) {
    $header = 'Location: '.gibShopURL().'/index.php?s='.$paymentInfoLink;
    header($header);
    exit();
} else {
    $payment = PaymentMethod::create('kPlugin_'.$oPlugin->kPlugin.'_'.$paymentName);
    $paymentWidgetUrl = $payment->getRecurringPaymentWidgetUrl();

    if (!$paymentWidgetUrl['isValid']) {
        $header = 'Location: '.gibShopURL().'/index.php?s='.$paymentInfoLink.'&sError='.$paymentWidgetUrl['response'];
        header($header);
        exit();
    } else {
        $sError = isset($_GET['sError']) ? filterXSS($_GET['sError']) : '';
        if ($sError) {
            $smarty->assign('sError', $oPlugin->oPluginSprachvariableAssoc_arr[$sError]);
        }
        $pluginPath = gibShopURL() . "/" . PFAD_PLUGIN . $oPlugin->cVerzeichnis
            . "/" . PFAD_PLUGIN_VERSION . $oPlugin->nVersion . "/paymentmethod";
        $smarty->assign('pluginPath', $pluginPath);
        $smarty->assign('paymentInfoLink', $paymentInfoLink);
        $smarty->assign('paymentName', $paymentName);
        $smarty->assign('lang_code', $payment->getLangCode());
        $smarty->assign('testMode', $payment->getTestMode());
        $smarty->assign('brand', $payment->getBrand());
        $smarty->assign('paymentWidgetUrl', $paymentWidgetUrl['response']);
        $smarty->assign('gibShopUrl', gibShopURL());
    }
}
