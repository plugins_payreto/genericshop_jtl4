<?php
include_once PFAD_ROOT . PFAD_INCLUDES_MODULES . 'PaymentMethod.class.php';
include_once(dirname(__FILE__).'/../paymentmethod/classes/payment/core.php');
include_once(dirname(__FILE__).'/../paymentmethod/classes/payment/Genericshop.php');

Jtllog::writeLog('debug log : start render genericshop checkout page', JTLLOG_LEVEL_ERROR);

if (empty($_SESSION['widgetData']) && !isset($_SESSION['widgetData'])) {
    Genericshop::redirectError('ERROR_GENERAL_NORESPONSE');
} else {
    global $oPlugin, $smarty;

    $widgetData = $_SESSION['widgetData'];
    Jtllog::writeLog('debug log : get widgetData : '.json_encode($widgetData), JTLLOG_LEVEL_ERROR);

    $smarty->assign('lang_code', $widgetData['lang_code']);
    $smarty->assign('testMode', $widgetData['testMode']);
    $smarty->assign('paymentMethod', $widgetData['paymentMethod']);
    $smarty->assign('brand', $widgetData['brand']);
    $smarty->assign('recurring', $widgetData['recurring']);
    $smarty->assign('redirect', $widgetData['redirect']);
    $smarty->assign('paymentWidgetUrl', $widgetData['paymentWidgetUrl']);
    $smarty->assign('registrations', $widgetData['registrations']);
    $smarty->assign('responseUrl', $widgetData['responseUrl']);
    $smarty->assign('cancelUrl', $widgetData['cancelUrl']);
    $smarty->assign('pluginPath', $widgetData['pluginPath']);
}
