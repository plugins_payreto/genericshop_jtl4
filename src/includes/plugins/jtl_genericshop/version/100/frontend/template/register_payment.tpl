<link href="{$pluginPath}/css/account_payment_form.css" rel="stylesheet" type="text/css">

<div id="content">

	    {if !empty($sError)}
			<div class="alert alert-danger">
				{$oPlugin->oPluginSprachvariableAssoc_arr.ERROR_MC_ADD} {$sError}
			</div>
		{/if}	

		{literal}
			<script type="text/javascript">
				function decode_utf8(s) {
				  return decodeURIComponent(escape(s));
				}
				
				var expiryDate = decode_utf8("Gültig bis");
				var cardNumberError = decode_utf8("Ungültige Kartennummer");
				var expiryMonthError = decode_utf8("Ungültiges Ablaufdatum");
				var expiryYearError = decode_utf8("Ungültiges Ablaufdatum");
				var cvvError = decode_utf8("Ungültige Kartenprüfnummer");
				var accountNumberIbanError = decode_utf8("Ungültige IBAN oder Kontonummer");
				var accountBankBicError = decode_utf8("Ungültige BIC (SWIFT-Code) oder Bankleitzahl");

				var buttonCancel = "<a href='index.php?s={/literal}{$paymentInfoLink}{literal}' class='wpwl-button btn_cancel'>{/literal}{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_BT_CANCEL}{literal}</a>";
				var buttonConfirm = "{/literal}{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_BT_REGISTER}{literal}";
				var ttRegistration = "<div class='register-tooltip'>{/literal}{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_TT_REGISTRATION}{literal}";
				var ttTestMode = "<div class='testmode'>{/literal}{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_TT_TESTMODE}{literal}";

				var wpwlOptions = {
					locale: "{/literal}{$lang_code}{literal}",
					{/literal}{if $lang_code == 'de'}{literal}
						labels: {expiryDate: expiryDate},
						errorMessages: {
							cardNumberError: cardNumberError,
							expiryMonthError: expiryMonthError,
							expiryYearError: expiryYearError,
							cvvError: cvvError,
							accountNumberIbanError: accountNumberIbanError,
							accountBankBicError: accountBankBicError
						},
					{/literal}{/if}{literal}
					style: "card",
					onReady: function(){
			            jQuery('form.wpwl-form').find('.wpwl-button').before(buttonCancel);
			            jQuery('.wpwl-button-pay').html(buttonConfirm);
						jQuery('.wpwl-container').after(ttRegistration);
			            {/literal}{if $testMode}{literal}
				            jQuery(".wpwl-container").wrap( "<div class='frametest'>");
				            jQuery('.wpwl-container').before(ttTestMode);   
			            {/literal}{/if}{literal}
					}
			    }
			</script>
		{/literal}
	<input type="submit" value="Submit" style="display:none" />
	{literal}
	<script>
	    jQuery(window).load(function() {
		    var jsElement = document.createElement("script");
		    jsElement.type = "text/javascript";
		    jsElement.src = "{/literal}{$paymentWidgetUrl}{literal}";
		    document.head.appendChild(jsElement);
	   });
	</script>
	{/literal}	
	<form action="{$gibShopUrl}/index.php?s={$paymentInfoLink}&pm={$paymentName}" class="paymentWidgets">{$brand}</form>
</div>
