{if $isRecurringActive}
	<link href="{$pluginPath}/css/account_payment_form.css" rel="stylesheet" type="text/css">

    {if !empty($sSuccess)}
		<div class="alert alert-success">
			{if $sSuccess == 'register'}
				{$oPlugin->oPluginSprachvariableAssoc_arr.SUCCESS_MC_ADD}
			{/if}
			{if $sSuccess == 'change' }
				{$oPlugin->oPluginSprachvariableAssoc_arr.SUCCESS_MC_UPDATE}
			{/if}
			{if $sSuccess == 'delete' }
				{$oPlugin->oPluginSprachvariableAssoc_arr.SUCCESS_MC_DELETE}
			{/if}
		</div>
	{/if}

    {if !empty($sError)}
		<div class="alert alert-danger">
			{if $action=='register'}
				{$oPlugin->oPluginSprachvariableAssoc_arr.ERROR_MC_ADD}
			{/if}
			{if $action=='change'}
				{$oPlugin->oPluginSprachvariableAssoc_arr.ERROR_MC_UPDATE}
			{/if}
			{$sError}
		</div>
	{/if}	

	{if $isCCSavedActive}
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<h3>{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_MC_CC}</h3>
			</div>
		</div>
		{foreach from=$customerDataCC item=list}
			<div class="row account-row">
				<div class="col-xs-12 col-sm-2 col-lg-2">
					<img src="{$pluginPath}/images/{$list.brand|lower}.png" class="brandImage" alt="{$list.brand}">
				</div>
				<div class="col-xs-12 col-sm-10 col-lg-4">
					<div class="well well-sm col-xs-12 col-sm-12 col-lg-12">
						{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_MC_ENDING} {$list.last4digits}; {$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_MC_VALIDITY} {$list.expiry_month}/{$list.expiry_year|substr:2}
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<div class="col-xs-12 col-sm-4 col-sm-offset-2 col-lg-6 col-lg-offset-0">
							{if $list.payment_default}
								<button class="btn btn-info account-btn">{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_MC_BT_DEFAULT}</button>
							{else}
								<form action="" method="post">
									<input type="hidden" name="id" value="{$list.id}"/>
									<input type="hidden" name="selected_payment" value="kreditkarte(recurring)"/>
									<input type="hidden" name="set_default" value="1"/>
									<button class="btn btn-info account-btn" type="submit" value="submit">{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_MC_BT_SETDEFAULT}</button>
								</form>	
							{/if}
						</div>
						<div class="col-xs-12 col-sm-3 col-lg-3">
							<form action="index.php?s={$changeLink}" method="post">
								<input type="hidden" name="id" value="{$list.id}"/>
								<input type="hidden" name="selected_payment" value="kreditkarte(recurring)"/>
								<button class="btn btn-info account-btn" type="submit" value="submit">{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_MC_BT_CHANGE}</button>
							</form>
						</div>
						<div class="col-xs-12 col-sm-3 col-lg-3">
							<form action="index.php?s={$deleteLink}" method="post">
								<input type="hidden" name="id" value="{$list.id}"/>
								<input type="hidden" name="selected_payment" value="kreditkarte(recurring)"/>
								<button class="btn btn-info account-btn" type="submit" value="submit">{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_MC_BT_DELETE}</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		{/foreach}
		<div class="row account-row">
			<div class="col-xs-12 col-sm-3 col-lg-2">
				<form action="index.php?s={$registerLink}" method="post">
					<input type="hidden" name="selected_payment" value="kreditkarte(recurring)"/>
					<button class="btn btn-info account-btn" type="submit" value="submit">{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_MC_BT_ADD}</button>
				</form>
			</div>
		</div>
	{/if}

{/if}
