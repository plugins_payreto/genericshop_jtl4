<div id="content">

	<link href="{$pluginPath}/css/account_payment_form.css" rel="stylesheet" type="text/css">

    {if !empty($sError)}
		<div class="alert alert-danger">
			{$oPlugin->oPluginSprachvariableAssoc_arr.ERROR_MC_DELETE} {$sError}
		</div>
	{/if}
	<p class="text-unreg">{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_MC_DELETESURE}</p>
	<form class="cancel_form" action="index.php?s={$paymentInfoLink}" method="post">
		<button type="submit" value="cancel" class="btn btn-danger">{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_BT_CANCEL}</button>
	</form>
	<form class="submit_form" action="" method="post">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="selected_payment" value="{$selected_payment}"/>		
		<button type="submit" name="doDelete" value="confirm" class="btn btn-info">{$oPlugin->oPluginSprachvariableAssoc_arr.FRONTEND_BT_CONFIRM}</button>
	</form>		
</div>
