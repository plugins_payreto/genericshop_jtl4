<?php

if (isset($_GET['i'])) {
    $cID = substr($GLOBALS['DB']->escape($_GET['i']), 1);

    $isDuringCheckout = isset($_GET['isDuringCheckout'])?$_GET['isDuringCheckout']:false;

    if ($isDuringCheckout) {
        $bestellid = $GLOBALS["DB"]->executeQuery(
            "select * from tzahlungsession where cZahlungsID='".$cID."'",
            1
        );
    } else {
        $bestellid   = $GLOBALS["DB"]->executeQuery(
            "SELECT ZID.kBestellung, ZA.cModulId
            FROM tzahlungsid ZID
            LEFT JOIN tzahlungsart ZA
                ON ZA.kZahlungsart = ZID.kZahlungsart
            WHERE ZID.cId = '" . $GLOBALS['DB']->escape($_GET['i']) . "'",
            1
        );
    }

    if ($bestellid->kBestellung > 0) {
        $bestellung = new Bestellung($bestellid->kBestellung);
        $bestellung->fuelleBestellung(0);
        $GLOBALS["DB"]->executeQuery(
            "delete from tzahlungsession where kBestellung=".$bestellid->kBestellung,
            4
        );
    }
    unset($_SESSION['widgetData']);
    $smarty->assign('Bestellung', $bestellung);
}
