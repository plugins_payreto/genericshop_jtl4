<?php

$GLOBALS["DB"]->executeQuery(
    "UPDATE tlink SET kLinkgruppe = 0 WHERE kPlugin = '".$oPlugin->kPlugin
    ."' AND cName in ('Save Payment Information', 'Change Payment Information', 'Delete Payment Information')",
    4
);

$isRecurringActive = $oPlugin->oPluginEinstellungAssoc_arr['genericshop_general_recurring'];

if ($isRecurringActive) {
    $GLOBALS["DB"]->executeQuery(
        "UPDATE tlink SET kLinkgruppe = 1 WHERE kPlugin = '".$oPlugin->kPlugin
        ."' AND cName = 'My Payment Information'",
        4
    );
} else {
    $GLOBALS["DB"]->executeQuery(
        "UPDATE tlink SET kLinkgruppe = 0 WHERE kPlugin = '"
        .$oPlugin->kPlugin."' AND cName = 'My Payment Information'",
        4
    );
}
