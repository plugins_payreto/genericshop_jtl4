<?php

include_once(dirname(__FILE__).'/../paymentmethod/classes/payment/core.php');
include_once(dirname(__FILE__).'/../paymentmethod/classes/payment/versiontracker.php');

global $oPlugin,$smarty,$DB;

$oPlugin = Plugin::getPluginById('jtl_genericshop');

$tLink = $DB->executeQuery("SELECT kLink FROM tlink WHERE kPlugin = '"
    .$oPlugin->kPlugin."' AND cName = 'My Payment Information'", 1);
$paymentInfoLink = $tLink->kLink;
$tLink = $DB->executeQuery("SELECT kLink FROM tlink WHERE kPlugin = '"
    .$oPlugin->kPlugin."' AND cName = 'Save Payment Information'", 1);
$registerLink = $tLink->kLink;
$tLink = $DB->executeQuery("SELECT kLink FROM tlink WHERE kPlugin = '"
    .$oPlugin->kPlugin."' AND cName = 'Change Payment Information'", 1);
$changeLink = $tLink->kLink;
$tLink = $DB->executeQuery("SELECT kLink FROM tlink WHERE kPlugin = '"
    .$oPlugin->kPlugin."' AND cName = 'Delete Payment Information'", 1);
$deleteLink = $tLink->kLink;

include_once(PFAD_ROOT . PFAD_INCLUDES_MODULES . 'PaymentMethod.class.php');

$setDefault = isset($_POST['set_default']) ? filterXSS($_POST['set_default']) : '';
$checkoutId = isset($_GET['id']) ? filterXSS($_GET['id']) : '';

if ($setDefault) {
    $id = isset($_POST['id']) ? filterXSS($_POST['id']) : '';
    $paymentName = isset($_POST['selected_payment']) ? filterXSS($_POST['selected_payment']) : '';
    $payment = PaymentMethod::create('kPlugin_'.$oPlugin->kPlugin.'_'.$paymentName);
    $payment->updateDefaultRegistration($id);
    $header = 'Location: ' . gibShopURL() . '/index.php?s='.$paymentInfoLink;
    header($header);
    exit();
} elseif ($checkoutId) {
    $paymentName = isset($_GET['pm']) ? filterXSS($_GET['pm']) : '';
    $recurringId = isset($_GET['recurringId']) ? filterXSS($_GET['recurringId']) : '';
    if ($recurringId) {
        $action = 'change';
    } else {
        $action = 'register';
    }

    $payment = PaymentMethod::create('kPlugin_'.$oPlugin->kPlugin.'_'.$paymentName);
    $result = $payment->finalizeRecurring($checkoutId, $recurringId);

    if ($result == 'success') {
        $header = 'Location: '.gibShopURL().'/index.php?s='.$paymentInfoLink.'&sSuccess='.$action;
    } else {
        if ($action == 'register') {
            $header = 'Location: '.gibShopURL().'/index.php?s='.$registerLink.'&selected_payment='.$paymentName
                .'&sError='.$result;
        } else {
            $header = 'Location: '.gibShopURL().'/index.php?s='.$changeLink.'&selected_payment='.$paymentName.'&id='
                .$recurringId.'&sError='.$result;
        }
    }
    header($header);
    exit();
}

$isRecurringActive = $oPlugin->oPluginEinstellungAssoc_arr['genericshop_general_recurring'];
$isCCSavedActive = $oPlugin->oPluginEinstellungAssoc_arr['kPlugin_'.$oPlugin->kPlugin
    .'_kreditkarte(recurring)_enabled'];

$smarty->assign('isRecurringActive', $isRecurringActive);
$smarty->assign('isCCSavedActive', $isCCSavedActive);

if ($isRecurringActive) {
    if ($isCCSavedActive) {
        $ccsaved = PaymentMethod::create('kPlugin_'.$oPlugin->kPlugin.'_kreditkarte(recurring)');
        $smarty->assign('customerDataCC', $ccsaved->getRegistrations());
    }
}

$smarty->assign('registerLink', $registerLink);
$smarty->assign('changeLink', $changeLink);
$smarty->assign('deleteLink', $deleteLink);

$pluginPath = gibShopURL() . "/" . PFAD_PLUGIN . $oPlugin->cVerzeichnis . "/"
    . PFAD_PLUGIN_VERSION . $oPlugin->nVersion . "/paymentmethod";
$smarty->assign('pluginPath', $pluginPath);

if (isset($_GET['sSuccess'])) {
    $smarty->assign('sSuccess', filterXSS($_GET['sSuccess']));
}
if (isset($_GET['sError'])) {
    $sError =filterXSS($_GET['sError']);
    $smarty->assign('sError', $oPlugin->oPluginSprachvariableAssoc_arr[$sError]);
}
if (isset($_GET['action'])) {
    $smarty->assign('action', filterXSS($_GET['action']));
}
