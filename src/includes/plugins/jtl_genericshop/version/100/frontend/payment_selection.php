<?php
include_once(dirname(__FILE__).'/../paymentmethod/classes/payment/core.php');

$ZahlungsartArr  = array();
$oZahlungsartArr = $smarty->get_template_vars('Zahlungsarten');

if (count($oZahlungsartArr) > 0) {
    foreach ($oZahlungsartArr as $key => $payment) {
        if ($payment->cAnbieter == "Genericshop") {
            $name = substr(
                $payment->cModulId,
                strripos($payment->cModulId, '_') + 1
            );
            $name = str_replace('(normal)', '', $name);
            $name = str_replace('(recurring)', 'saved', $name);
            $name = str_replace('kreditkarte', 'cc', $name);

            $paymentMethod = 'genericshop_'.$name;

            if ($oPlugin->oPluginEinstellungAssoc_arr['genericshop_general_recurring']
                == 1) {
                if ($_SESSION['Kunde']->kKunde) {
                    if ($paymentMethod == 'genericshop_cc') {
                        Shop::DB()->query("UPDATE tzahlungsart SET nNutzbar=0 where cModulId='".
                                $payment->cModulId."'", 2);
                        $paymentName = str_replace('(normal)', '(recurring)', $payment->cModulId);
                        Shop::DB()->query("UPDATE tzahlungsart SET nNutzbar=".
                                $oPlugin->oPluginEinstellungAssoc_arr[$paymentName.'_enabled'].
                                " where cModulId='".$paymentName."'", 2);
                        continue;
                    }
                } else {
                    if ($paymentMethod == 'genericshop_ccsaved') {
                        Shop::DB()->query("UPDATE tzahlungsart SET nNutzbar=0 where cModulId='".
                                $payment->cModulId."'", 2);
                        $paymentName = str_replace('(recurring)', '(normal)', $payment->cModulId);
                        Shop::DB()->query("UPDATE tzahlungsart SET nNutzbar=".
                                $oPlugin->oPluginEinstellungAssoc_arr[$paymentName.'_enabled'].
                                " where cModulId='".$paymentName."'", 2);
                        continue;
                    }
                }
            } else {
                if ($paymentMethod == 'genericshop_ccsaved') {
                    Shop::DB()->query("UPDATE tzahlungsart SET nNutzbar=0 where cModulId='".
                            $payment->cModulId."'", 2);
                    $paymentName = str_replace('(recurring)', '(normal)', $payment->cModulId);
                    Shop::DB()->query("UPDATE tzahlungsart SET nNutzbar=".
                            $oPlugin->oPluginEinstellungAssoc_arr[$paymentName.'_enabled'].
                            " where cModulId='".$paymentName."'", 2);
                    continue;
                }
            }

            if (!strpos($payment->cModulId, '(normal)') ||
                    !strpos($payment->cModulId, '(recurring)') ||
                    !strpos($payment->cModulId, '(separecurring)') ||
                    !strpos($payment->cModulId, '(sepa)')) {
                Shop::DB()->query("UPDATE tzahlungsart SET nNutzbar=".
                        $oPlugin->oPluginEinstellungAssoc_arr[$payment->cModulId.
                            '_enabled']." where cModulId='".$payment->cModulId."'", 2);
            }
            if ($oPlugin->oPluginEinstellungAssoc_arr[$payment->cModulId.'_enabled']
                == 0) {
                continue;
            }


            if ($paymentMethod == 'genericshop_cc' || $paymentMethod == 'genericshop_ccsaved') {
                $path           = $oPlugin->cFrontendPfadURL.'../paymentmethod/images/';
                $payment->cBild = '';
                $cardBrands     = array('visa', 'master', 'amex', 'diners', 'jcb');
                $cardLogos      = array();
                foreach ($cardBrands as $brand) {
                    if ($oPlugin->oPluginEinstellungAssoc_arr[$payment->cModulId.'_'.$brand]) {
                        $cardLogos[] = $brand;
                    }
                }
                if (empty($cardLogos)) {
                    continue;
                } else {
                    $count = count($cardLogos);
                    foreach ($cardLogos as $key => $logo) {
                        if ($key > 0) {
                            $payment->cBild .= '<img src="';
                        }
                        if ($key < $count - 1) {
                            $payment->cBild .= $path.$logo.'.png" alt="'.
                                $payment->angezeigterName[$_SESSION['cISOSprache']].
                                '" class="vmiddle" height="49" style="margin-right: 5px" />';
                        } else {
                            $payment->cBild .= $path.$logo.
                            '.png" class="vmiddle" style="height : 34px '.
                            '!important;"><img style="display : none;" src="';
                        }
                    }
                    Shop::DB()->query('UPDATE tzahlungsart SET cBild="'.
                            Shop::DB()->escape($payment->cBild).'" where cModulId="'.$payment->cModulId.'"', 2);
                }
            } else {
                $payment->cBild .= '" height="49" class="vmiddle"><img style="display : none;" src="';
            }
        }
        $ZahlungsartArr[] = $payment;
    }

    $smarty->assign('Zahlungsarten', $ZahlungsartArr);
}
