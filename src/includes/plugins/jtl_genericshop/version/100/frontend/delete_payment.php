<?php

include_once(dirname(__FILE__).'/../paymentmethod/classes/payment/core.php');

global $oPlugin,$smarty,$DB;

$oPlugin = Plugin::getPluginById('jtl_genericshop');

$tLink = $DB->executeQuery("SELECT kLink FROM tlink WHERE kPlugin = '"
    .$oPlugin->kPlugin."' AND cName = 'My Payment Information'", 1);
$paymentInfoLink = $tLink->kLink;

include_once(PFAD_ROOT . PFAD_INCLUDES_MODULES . 'PaymentMethod.class.php');

$paymentName = isset($_REQUEST['selected_payment']) ? filterXSS($_REQUEST['selected_payment']) : '';
$id = isset($_REQUEST['id']) ? filterXSS($_REQUEST['id']) : '';
$doDelete = isset($_POST['doDelete']) ? filterXSS($_POST['doDelete']): '';

$payment = PaymentMethod::create('kPlugin_'.$oPlugin->kPlugin.'_'.$paymentName);

if ($doDelete) {
    $result = $payment->deleteRecurring($id);
    if ($result == "success") {
        $header = 'Location: '.gibShopURL().'/index.php?s='.$paymentInfoLink.'&sSuccess=delete';
        header($header);
        exit();
    } else {
        $smarty->assign(
            'sError',
            $oPlugin->oPluginSprachvariableAssoc_arr[$result]
        );
    }
}

$smarty->assign('paymentInfoLink', $paymentInfoLink);
$pluginPath = gibShopURL() . "/" . PFAD_PLUGIN . $oPlugin->cVerzeichnis
    . "/" . PFAD_PLUGIN_VERSION . $oPlugin->nVersion . "/paymentmethod";
$smarty->assign('pluginPath', $pluginPath);

$smarty->assign('selected_payment', $paymentName);
$smarty->assign('id', $id);
